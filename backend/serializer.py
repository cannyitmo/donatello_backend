from rest_framework import serializers
from .models import User, Merchant, Organization, Donation
import json

class UserSerializer(serializers.ModelSerializer):
    accounts = serializers.SerializerMethodField()

    def get_accounts(self, model_instance):
        try:
            if model_instance.userBankAccounts == '':
                return json.loads("[]")
            return json.loads(model_instance.userBankAccounts)
        except (TypeError, ValueError):
            print("Failed to convert userBankAccounts to JSON")
            return "None"

    class Meta:
        model = User
        fields = (
            "userId",
            "username",
            "accounts",
            "fullName"
        )

class MerchantSerializer(serializers.ModelSerializer):
    organization = serializers.SerializerMethodField()

    def get_organization(self, model_instance):
        try:
            snippet = Organization.objects.get(pk=model_instance.organizationId)
            serializer = OrganizationSerializer(snippet)
            return serializer.data
        except Organization.DoesNotExist:
            print("Failed to get organization")
            return "None"

    class Meta:
        model = Merchant
        fields = (
            "merchantId",
            "displayName",
            "thumbnailURL",
            "donatedBy",
            "organization"
        )

class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = (
            "organizationId",
            "displayName",
            "thumbnailURL",
            "donatedTo",
            "acronym"
        )

class DonationSerializer(serializers.ModelSerializer):
    merchant = serializers.SerializerMethodField()
    organization = serializers.SerializerMethodField()

    def get_merchant(self, model_instance):
        try:
            snippet = Merchant.objects.get(pk=model_instance.merchantId)
            serializer = MerchantSerializer(snippet)
            return serializer.data
        except Merchant.DoesNotExist:
            print("Failed to get merchant")
            return "None"

    def get_organization(self, model_instance):
        try:
            snippet = Organization.objects.get(pk=model_instance.organizationId)
            serializer = OrganizationSerializer(snippet)
            return serializer.data
        except Organization.DoesNotExist:
            print("Failed to get organization")
            return "None"
    class Meta:
        model = Donation
        fields = (
            "donationId",
            "isCompleted",
            "amount",
            "merchant",
            "organization",
            "creationDate"
        )
